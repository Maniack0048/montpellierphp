<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('pack_name');
            $table->string('description');
            $table->string('main_image');
            $table->integer('price_from');
            $table->date('start_date');
            $table->date('end_date');
            $table->enum('pack_type',['Nacional','Internacional']);
            $table->boolean('outstanding');
            $table->integer('duration_days');
            $table->integer('duration_nights');
            $table->boolean('pack_status')->default(1);
            $table->integer('pack_category_id');
            $table->integer('catalog_id');
            $table->string('page_number');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
}
