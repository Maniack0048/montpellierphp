<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Catalog extends Model
{
    protected $fillable=['catalog_name','catalog_image','created_at','updated_at'];
}
