<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
   protected $fillable=['hotel_name','hotel_address','web_page','created_at','updated_at'];
}
