<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Package;
use App\Catalog;
use App\CategoryPackage;

class PackageController extends Controller
{

      public function __construct()
    {
        $this->middleware('auth')->except('logout');
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        

        $packages =Package::select('packages.id','packages.pack_name', 'packages.description','packages.main_image','packages.price_from','packages.start_date','packages.end_date','packages.pack_type','packages.outstanding','packages.duration_days','packages.duration_nights','packages.pack_status','packages.page_number','category_packages.category_pack_name','catalogs.catalog_name')
                ->join('category_packages', 'packages.pack_category_id', '=', 'category_packages.id')
                 ->join('catalogs', 'packages.catalog_id', '=', 'catalogs.id')
                ->get();




        return view('admin.indexPackages',compact('packages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $catalogs=Catalog::all();
        $categories=CategoryPackage::all();
        return view('admin.packageForm',compact('catalogs','categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        

$package=(new Package)->fill($request->all());
 
   $package->main_image=  $request->file('main_image')->store('/montpellier/storage/app/');

   

       $package->save();

        return redirect()->route('packages.index');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $package=Package::findOrFail($id);
                $catalogs=Catalog::all();
        $categories=CategoryPackage::all();
        return view('admin.packageEdit',compact('package','catalogs','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $package=Package::findOrFail($id);

if($request->hasFile('main_image')){

 $package->main_image=  $request->file('main_image')->store('/montpellier/storage/app/');
    }

      $package->update($request->only('pack_name','description','price_from','start_date','end_date','pack_type','outstanding','duration_days','duration_nights','pack_category_id','catalog_id','page_number'));

           return redirect()->route('packages.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Package::findOrFail($id)->delete();
        return redirect()->route('packages.index');
    }
}
