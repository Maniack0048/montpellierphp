<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Airline;

class AirlineController extends Controller
{



  public function __construct()
    {
        $this->middleware('auth')->except('logout');
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


       $airlines=Airline::all();
        return view('admin.indexAirlines',compact('airlines'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.airlineForm');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

$airline=(new Airline)->fill($request->all());
 
   $airline->airline_photo=  $request->file('airline_photo')->store('/montpellier/storage/app/');

       $airline->save();

        return redirect()->route('airlines.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $airline=Airline::findOrFail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
          $airline=Airline::findOrFail($id);

          return view('admin.airlineEdit',compact('airline'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         
$airline=Airline::findOrFail($id);

if($request->hasFile('airline_photo')){

 $airline->airline_photo=  $request->file('airline_photo')->store('/montpellier/storage/app/');

}


       $airline->update($request->only('airline_name','airline_webpage','roun_flidtht'));

           return redirect()->route('airlines.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Airline::findOrFail($id)->delete();

        return redirect()->route('airlines.index');
    }
}
