<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

class PacksController extends Controller
{
    public function store(){

    	$this->validate(request(),[
			'stDate' => 'required',
			'enDate' => 'required'
    	]);

    	$name = request('packName');
    	$type = request('packType');
    	$stDate = request('stDate');
    	$enDate = request('enDate');
    	$min = request('minPr');
    	$max = request('maxPr');

    	$packages = DB::table('packages')
    	->where('pack_name','like','%'.$name.'%')
    	->where('pack_type',$type)
    	->whereBetween('price_from',[$min,$max])
    	->whereBetween('start_date',[date('Y-m-d',strtotime($stDate)),date('Y-m-d',strtotime($enDate))])
    	->whereDate('start_date','>=','2019-03-29')
    	->get();

		 return view('packages',compact('packages'));
    }
}
