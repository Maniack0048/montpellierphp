<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Catalog;

class CatalogController extends Controller
{

      public function __construct()
    {
        $this->middleware('auth')->except('logout');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $catalogs=Catalog::all();
        return view('admin.indexCatalogs',compact('catalogs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.catalogForm');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $catalog=(new Catalog)->fill($request->all());

        $catalog->catalog_image=$request->file('catalog_image')->store('/montpellier/storage/app/');
        $catalog->save();

        return redirect()->route('catalogs.index');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $catalog=Catalog::findOrFail($id);
        return view('admin.catalogEdit',compact('catalog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $catalog=Catalog::findOrFail($id);

        if($request->hasFile('catalog_image')){

            $catalog->catalog_image=$request->file('catalog_image')->store('/montpellier/storage/app/');
        }
        $catalog->update($request->only('catalog_name'));

        return redirect()->route('catalogs.index');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Catalog::findOrFail($id)->delete();

        return redirect()->route('catalogs.index');
    }
}
