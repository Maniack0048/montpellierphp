<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryPackage extends Model
{
    protected $fillable=['category_pack_name','created_at','updated_at'];
}
