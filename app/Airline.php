<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Airline extends Model
{
    protected $fillable=['airline_name','airline_photo','airline_webpage','round_flidht','created_at','updated_at'];
}
