<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	$importants = DB::table('packages')->where('outstanding','1')->get();
	$packages = DB::table('packages')->paginate(6);
    return view('welcome',compact('packages','importants'));
})->name('index');

Route::get('packs','PacksController@store')->name('packs');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::resource('hotels','HotelController');

Route::resource('airlines','AirlineController');

Route::resource('services','ServiceController');

Route::resource('packages','PackageController');

Route::resource('catalogs','CatalogController');

Route::resource('categories','CategoryPackageController');

Route::resource('users','UserController');