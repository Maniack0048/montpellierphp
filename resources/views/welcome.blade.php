@extends('layout')

@section('content')

<script>

function cambiarValers(valor){
  document.getElementById("min").value=valor;
}
function cambiarValers2(valor){
  document.getElementById("max").value=valor;
}

  </script>

  <div class="hero-wrap js-fullheight" style="background-image: url('assets/images/bg_1.jpg');">
    <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text js-fullheight align-items-center justify-content-start" data-scrollax-parent="true">
          <div class="col-md-9 ftco-animate" data-scrollax=" properties: { translateY: '70%' }">
            <h1 class="mb-4" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }"><strong>Bienvenido a <br></strong> Montpellier Travel Tours</h1>
            <p data-scrollax="properties: { translateY: '30%', opacity: 1.6 }">Sueñalo, vívelo & disfrutalo</p>
          </div>
        </div>
      </div>
    </div>

    <section class="ftco-section services-section bg-light">
      <div class="container">
        <div class="row d-flex">
          <div class="col-md-3 d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services d-block text-center">
              <div class="d-flex justify-content-center"><div class="icon"><span class="flaticon-guarantee"></span></div></div>
              <div class="media-body p-2 mt-2">
                <h3 class="heading mb-3">El mejor precio garantizado</h3>
                <p>Te ofrecemos las mejores promociones para que te sea fácil viajar.</p>
              </div>
            </div>      
          </div>
          <div class="col-md-3 d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services d-block text-center">
              <div class="d-flex justify-content-center"><div class="icon"><span class="flaticon-like"></span></div></div>
              <div class="media-body p-2 mt-2">
                <h3 class="heading mb-3">Clientes satisfechos</h3>
                <p>Nuestra calidad y servicio siempre brindan la mejor experiencia.</p>
              </div>
            </div>    
          </div>
          <div class="col-md-3 d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services d-block text-center">
              <div class="d-flex justify-content-center"><div class="icon"><span class="flaticon-detective"></span></div></div>
              <div class="media-body p-2 mt-2">
                <h3 class="heading mb-3">La mejor agencia de viajes</h3>
                <p>Nuestra experiencia ha logrado encontrar las mejores promociones para ti.</p>
              </div>
            </div>      
          </div>
          <div class="col-md-3 d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services d-block text-center">
              <div class="d-flex justify-content-center"><div class="icon"><span class="flaticon-support"></span></div></div>
              <div class="media-body p-2 mt-2">
                <h3 class="heading mb-3">Atención personalizada</h3>
                <p>Contamos con las mejores opciones acorde a tus gustos.</p>
              </div>
            </div>      
          </div>
        </div>
      </div>
    </section>

    <section class="ftco-section ftco-destination">
      <div class="container">
        <div class="row justify-content-start mb-5 pb-3">
          <div class="col-md-7 heading-section ftco-animate">
            <span class="subheading">Destacado</span>
            <h2 class="mb-4"><strong>Mejores</strong> promociones</h2>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="destination-slider owl-carousel ftco-animate">
              @foreach($importants as $important)
              <div class="item">
                <div class="destination">
                  <a href="#" class="img d-flex justify-content-center align-items-center" style="background-image: url(/montpellier/storage/app/{{$important->main_image}});">
                    <div class="icon d-flex justify-content-center align-items-center">
                      <span class="icon-search2"></span>
                    </div>
                  </a>
                  <div class="text p-3">
                    <div class="d-flex">
                      <div class="one">
                        <h3><a href="#">{{$important->pack_name}}</a></h3>
                        <p class="rate">
                          <i class="icon-star"></i>
                          <i class="icon-star"></i>
                          <i class="icon-star"></i>
                          <i class="icon-star"></i>
                          <i class="icon-star-o"></i>
                          <span>8 Rating servicios</span>
                        </p>
                      </div>
                      <div class="two">
                        <span class="price">${{ $important->price_from }}</span>
                      </div>
                    </div>
                    <p>{{$important->description}}</p>
                    <p class="days"><span>{{$important->duration_days}} days {{$important->duration_nights}} nights</span></p>
                    <hr>
                    <p class="bottom-area d-flex">
                      <span class="ml-auto"><a href="#"><i class="icon-markunread_mailbox"></i>Cotizar</a></span>
                      <span class="ml-auto"><a href="#">Discover</a></span>
                      <span class="ml-auto"><a href="https://wa.me/5215537056821?text=Me%20gustaría%20saber%20el%20precio%20del%20paquete%20{{$important->pack_name}}"><i class="fab fa-whatsapp"></i></a></span>
                    </p>
                  </div>
                </div>
              </div>
              @endforeach
            </div>
          </div>
        </div>
      </div>
    </section>


     <section class="ftco-section ftco-degree-bg">
      <div class="container">
        <div class="row">
          <div class="col-lg-3 sidebar ftco-animate">
            <div class="sidebar-wrap bg-light ftco-animate">
              <h3 class="heading mb-4">Find City</h3>
              <form action="{{ route('packs') }}" method="GET">
                @csrf
                <div class="fields">
                  <div class="form-group">
                    <input name="packName" type="text" class="form-control" placeholder="País, ciudad">
                  </div>
                  <div class="form-group">
                    <div class="select-wrap one-third">
                      <div class="icon"><span class="ion-ios-arrow-down"></span></div>
                      <select name="packType" id="" class="form-control" placeholder="Keyword search">
                        <option value="Nacional">Nacional</option>
                        <option value="Internacional">Internacional</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <input name="stDate" type="text" id="checkin_date" class="form-control" placeholder="Fecha desde">
                    <p style="color: red;"> {{$errors->first('stDate')}} </p>
                  </div>
                  <div class="form-group">
                    <input name="enDate" type="text" id="checkin_date" class="form-control" placeholder="Fecha hasta">
                    <p style="color: red;"> {{$errors->first('enDate')}} </p>
                  </div>

                  <div class="form-group">
                    <div class="range-slider">
                      <span>
                        <input type="number" value="1000" min="0" max="120000" id="min" disabled /> -
                        <input type="number" value="50000" min="0" max="120000" id="max" disabled />
                      </span>

                      <input name="minPr" value="1000" min="0" max="50000" step="500" type="range" onchange="cambiarValers(this.value)" />
                      <input name="maxPr" value="50000" min="0" max="50000" step="500" type="range" onchange="cambiarValers2(this.value)"/>
                      </svg>
                    </div>
                  </div>
                  <div class="form-group">
                    <input type="submit" value="Buscar" class="btn btn-primary py-3 px-5">
                  </div>
                </div>
              </form>
            </div>
            <div class="sidebar-wrap bg-light ftco-animate">
              <h3 class="heading mb-4">Paquetes</h3>
              <form method="post" class="star-rating">
                <div class="form-check">
                  <input type="checkbox" class="form-check-input" id="exampleCheck1">
                  <label class="form-check-label" for="exampleCheck1">
                    Nacionales
                  </label>
                </div>
                <div class="form-check">
                  <input type="checkbox" class="form-check-input" id="exampleCheck1">
                  <label class="form-check-label" for="exampleCheck1">
                     Internacionales
                  </label>
                </div>
              </form>
            </div>
          </div>
          <div class="col-lg-9">
            <div class="row">
              @foreach($packages as $pack)
              <div class="col-md-4 ftco-animate">
                <div class="destination">
                  <a href="#" class="img img-2 d-flex justify-content-center align-items-center" style="background-image: url(/montpellier/storage/app/{{$pack->main_image}});">
                    <div class="icon d-flex justify-content-center align-items-center">
                  <span class="icon-search2"></span>
                </div>
                  </a>
                  <div class="text p-3">
                    <div class="d-flex">
                      <div class="one">
                        <h3><a href="#">{{ $pack->pack_name }}</a></h3>
                        <p class="rate">
                          <i class="icon-star"></i>
                          <i class="icon-star"></i>
                          <i class="icon-star"></i>
                          <i class="icon-star"></i>
                          <i class="icon-star-o"></i>
                          aqui van los servs
                          <span>8 Rating</span>
                        </p>
                      </div>
                      <div class="two">
                        <span class="price">${{$pack->price_from}}</span>
                      </div>
                    </div>
                    <p>{{$pack->description}}</p>
                    <p class="days"><span>{{$pack->duration_days}} days {{$pack->duration_nights}} nights</span></p>
                    <hr>
                    <p class="bottom-area d-flex">
                      <span class="ml-auto"><a href="#"><i class="icon-markunread_mailbox"></i>Cotizar</a></span>
                      <span class="ml-auto"><a href="#">Discover</a></span>
                      <span class="ml-auto"><a href="https://wa.me/5215537056821?text=Me%20gustaría%20saber%20el%20precio%20del%20paquete%20{{$pack->pack_name}}"><i class="fab fa-whatsapp"></i></a></span>
                    </p>
                  </div>
                </div>
              </div>
              @endforeach
            </div>
            <div class="row mt-5">
              <div class="col text-center">
                <div class="block-27"><center>
                  {!! $packages->links() !!}
                </center></div>
              </div>
            </div>
          </div> <!-- .col-md-8 -->
        </div>
      </div>
    </section> <!-- .section -->

@endsection

