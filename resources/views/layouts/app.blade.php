<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Montpellier') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Montpellier') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                @if(auth()->check())
                    <ul class="navbar-nav mr-auto">
                          <li class="nav-item dropdown" >

                             <a id="navbarDropdownHotel" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                   {{ __('Hoteles') }} <span class="caret"></span>
                                </a>

                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownHotel">
                                    <a class="dropdown-item" href="{{ route('hotels.create') }}">
                                        {{ __('Registrar Hotel') }}
                                    </a>   <a class="dropdown-item" href="{{ route('hotels.index') }}">
                                        {{ __('Ver Hoteles') }}
                                    </a>
                          </li>



                          <li class="nav-item dropdown" >

                             <a id="navbarDropdownAirline" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                   {{ __('Aerolineas') }} <span class="caret"></span>
                                </a>

                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownAirline">
                                    <a class="dropdown-item" href="{{ route('airlines.create') }}">
                                        {{ __('Registrar Aerolinea') }}
                                    </a>   <a class="dropdown-item" href="{{ route('airlines.index') }}">
                                        {{ __('Ver Aerolineas') }}
                                    </a>
                          </li>


    <ul class="navbar-nav mr-auto">
                          <li class="nav-item dropdown" >

                             <a id="navbarDropdownUser" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                   {{ __('Usuarios') }} <span class="caret"></span>
                                </a>

                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownUser">
                                    <a class="dropdown-item" href="{{ route('register') }}">
                                        {{ __('Registrar Usuarios') }}
                                    </a>
                                    <a class="dropdown-item" href="{{ route('users.index') }}">
                                        {{ __('Ver Usuarios') }}
                                    </a>   
                                
                          </li>




                              <ul class="navbar-nav mr-auto">
                          <li class="nav-item dropdown" >

                             <a id="navbarDropdownService" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                   {{ __('Servicios') }} <span class="caret"></span>
                                </a>

                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownService">
                                    <a class="dropdown-item" href="{{ route('services.create') }}">
                                        {{ __('Registrar Servicio') }}
                                    </a>   <a class="dropdown-item" href="{{ route('services.index') }}">
                                        {{ __('Ver Servicios') }}
                                    </a>
                          </li>

                    </ul>


                    
                              <ul class="navbar-nav mr-auto">
                          <li class="nav-item dropdown" >

                             <a id="navbarDropdownPaquetes" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                   {{ __('Paquetes') }} <span class="caret"></span>
                                </a>

                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPaquetes">
                                    <a class="dropdown-item" href="{{ route('packages.create') }}">
                                        {{ __('Registrar Paquetes') }}
                                    </a>   <a class="dropdown-item" href="{{ route('packages.index') }}">
                                        {{ __('Ver Paquetes') }}
                                    </a>
                          </li>





                          
                    </ul>
                
                              <ul class="navbar-nav mr-auto">
                          <li class="nav-item dropdown" >

                             <a id="navbarDropdownCatalogs" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                   {{ __('Catalogos') }} <span class="caret"></span>
                                </a>

                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownCatalogs">
                                    <a class="dropdown-item" href="{{ route('catalogs.create') }}">
                                        {{ __('Registrar Catalogo') }}
                                    </a>   <a class="dropdown-item" href="{{ route('catalogs.index') }}">
                                        {{ __('Ver Catalogos') }}
                                    </a>
                          </li>

                    </ul>

                               <ul class="navbar-nav mr-auto">
                          <li class="nav-item dropdown" >

                             <a id="navbarDropdownCategory" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                   {{ __('Categorias de Paquetes') }} <span class="caret"></span>
                                </a>

                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownCategory">
                                    <a class="dropdown-item" href="{{ route('categories.create') }}">
                                        {{ __('Registrar Categoria') }}
                                    </a>   <a class="dropdown-item" href="{{ route('categories.index') }}">
                                        {{ __('Ver Categorias') }}
                                    </a>
                          </li>

                    </ul>


                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                    
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        
                    </ul>

                    @endif
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
