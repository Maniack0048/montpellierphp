@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Actualizacion de Paquetes') }}</div>

                <div class="card-body">
                    <form method="POST" enctype="multipart/form-data" action="{{ route('packages.update',$package->id) }}">
                              {!! csrf_field() !!}
          {!! method_field('PUT') !!}


                        <div class="form-group row">
                            <label for="pack_name" class="col-md-4 col-form-label text-md-right">{{ __('Nombre del paquete') }}</label>

                            <div class="col-md-6">
                                <input id="pack_name" type="text" class="form-control{{ $errors->has('pack_name') ? ' is-invalid' : '' }}" name="pack_name" value="{{ $package->pack_name }}" required autofocus>

                                @if ($errors->has('pack_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('pack_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="description" class="col-md-4 col-form-label text-md-right">{{ __('Descripcion') }}</label>

                            <div class="col-md-6">
                                <input id="description" type="text" class="form-control" name="description" value="{{$package->description}}" required>
                            </div>
                        </div>

  						<div class="form-group row">
                            <label for="main_image" class="col-md-4 col-form-label text-md-right">{{ __('Imagen Principal') }}</label>

                            <div class="col-md-6">
                                <input type="file" id="main_image" type="text" class="form-control" name="main_image" >
                            </div>
                        </div>

						<div class="form-group row">
                            <label for="price_from" class="col-md-4 col-form-label text-md-right">{{ __('Precio desde') }}</label>

                            <div class="col-md-6">
                                <input id="price_from" type="text" class="form-control" name="price_from" value="{{$package->price_from}}" required>
                            </div>
                        </div>


<div class="form-group row">
   <label for="start_date" class="col-md-4 col-form-label text-md-right">{{ __('Fecha Inicial') }}</label>

 
  <div class="col-md-6">
    <input class="form-control" type="date" value="{{$package->start_date}}" id="start_date" name="start_date">
  </div>
</div>


<div class="form-group row">
   <label for="end_date" class="col-md-4 col-form-label text-md-right">{{ __('Fecha Fin') }}</label>

 
  <div class="col-md-6">
    <input class="form-control" type="date" value="{{$package->end_date}}" id="end_date" name="end_date">
  </div>
</div>


                    <div class="form-group row">
                            <label for="package_type" class="col-md-4 col-form-label text-md-right">{{ __('Tipo de paquete') }}</label>

                            <div class="col-md-6">
                            <select id="package_type" type="text" class="form-control" name="package_type" >
                                @if($package->package_type=='Nacional')
                                <option value="Nacional" selected="selected">Nacional</option>
                                <option value="Internacional">Internacional</option>
                                @else
                                 <option value="Nacional">Nacional</option>
                                <option value="Internacional" selected="selected">Internacional</option>
                                @endif
                            </select>
                            </div>
                        </div>


                    <div class="form-group row">
                            <label for="outstanding" class="col-md-4 col-form-label text-md-right">{{ __('Destacado') }}</label>

                            <div class="col-md-6">
                            <select id="outstanding" type="text" class="form-control" name="outstanding" >

                                @if($package->outstanding==1)
                                <option >Seleccione</option>
                                <option value="1" selected="selected">Si</option>
                                <option value="0">No</option>

                                @else

                                 <option >Seleccione</option>
                                <option value="1">Si</option>
                                <option value="0" selected="selected">No</option>
                                @endif
                            </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="duration_days" class="col-md-4 col-form-label text-md-right">{{ __('Duracion de dias') }}</label>

                            <div class="col-md-6">
                                <input id="duration_days" type="number" class="form-control" name="duration_days" value="{{$package->duration_days}}" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="duration_nights" class="col-md-4 col-form-label text-md-right">{{ __('Duracion de noches') }}</label>

                            <div class="col-md-6">
                                <input id="duration_nights" type="number" class="form-control" name="duration_nights" value="{{$package->duration_nights}}" required>
                            </div>
                        </div>


                    <div class="form-group row">
                            <label for="pack_category_id" class="col-md-4 col-form-label text-md-right">{{ __('Categoria') }}</label>

                            <div class="col-md-6">
                            <select id="pack_category_id" type="text" class="form-control" name="pack_category_id" >
                                <option >Seleccione</option>

                                @if(isset($categories))
                            

                                @foreach($categories as $category) 




                                        <option value="{{$category->id}}"  @if($category->id==$package->pack_category_id) selected="selected"@endif   >{{$category->category_pack_name}}</option>
                                     @endforeach

                                @else

                                          <option >Sin datos</option>

                               @endif


                            </select>
                            </div>
                        </div>

                    <div class="form-group row">
                            <label for="catalog_id" class="col-md-4 col-form-label text-md-right">{{ __('Catalogo') }}</label>

                            <div class="col-md-6">
                            <select id="catalog_id" type="text" class="form-control" name="catalog_id" >
                                <option >Seleccione</option>


                                @if(isset($catalogs))
                            
                           @foreach($catalogs as $catalog) 
                                     $image=$catalog->catalog_image;
                               <option value="{{$catalog->id}}"  @if($catalog->id==$package->catalog_id) selected="selected"@endif    >{{$catalog->catalog_name}}</option>

                                 
                               @endforeach
                                @else
                                    <option >Sin datos</option>

                               @endif
                            </select>
                            </div>
                        </div>                                              
<div class="form-group row">

   @if(@isset($image))
    <img src="{{$image}}">


   @else
   <img src="">
    @endif
</div>

                        <div class="form-group row">
                            <label for="page_number" class="col-md-4 col-form-label text-md-right">{{ __('Numero de pagina') }}</label>

                            <div class="col-md-6">
                                <input id="page_number" type="text" class="form-control" name="page_number" value="{{$package->page_number}}" required>
                            </div>
                        </div>





             			<div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Actualizar') }}
                                </button>
                            </div>
                        </div>



                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection