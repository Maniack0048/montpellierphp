@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">

<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Nombre</th>
      <th scope="col">Direccion</th>
      <th scope="col">Pagina web</th>
 
      <th scope="col">Acciones</th>
    </tr>
  </thead>
  <tbody>
  	@foreach($hotels as $hotel)

    <tr>
      <td>{{$hotel->hotel_name}}</td>
      <td>{{$hotel->hotel_address}}</td>
      <td>{{$hotel->web_page}}</td>
  
      <td>
        <a class="btn btn-info btn-xs" href="{{route('hotels.edit',$hotel->id)}}">Editar</a>

        <form style="display:inline" method="POST" action="{{route('hotels.destroy',$hotel->id)}}">
          {!! csrf_field() !!}
          {!! method_field('DELETE') !!}
          
          <button class="btn btn-danger" type="submit">Eliminar</button>


        </form>
      </td>



    </tr>

    @endforeach
</tbody>
</table>
</div>
</div>
@endsection