@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row justify-content-center">
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Nombre</th>
      <th scope="col">Descripción</th>
      <th scope="col">Icono</th>
      <th scope="col">Acciones</th>
      
    </tr>
  </thead>
  <tbody>
  	@foreach($services as $service)

    <tr>
      <td>{{$service->service_name}}</td>
      <td>{{$service->service_description}}</td>
       <td><img src="/montpellier/storage/app/{{($service->service_icon)}}"></td>

          <td>
        <a class="btn btn-info btn-xs" href="{{route('services.edit',$service->id)}}">Editar</a>

        <form style="display:inline" method="POST" action="{{route('services.destroy',$service->id)}}">
          {!! csrf_field() !!}
          {!! method_field('DELETE') !!}
          
          <button class="btn btn-danger" type="submit">Eliminar</button>


        </form>
      </td>
     

    </tr>

    @endforeach
</tbody>
</table>
</div>
</div>
@endsection