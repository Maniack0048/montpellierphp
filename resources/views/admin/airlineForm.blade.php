@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Registro de Aerolinea') }}</div>

                <div class="card-body">
                    <form method="POST" enctype="multipart/form-data" action="{{ route('airlines.store') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="airline_name" class="col-md-4 col-form-label text-md-right">{{ __('Nombre de la Aerolinea') }}</label>

                            <div class="col-md-6">
                                <input id="airline_name" type="text" class="form-control{{ $errors->has('airline_name') ? ' is-invalid' : '' }}" name="airline_name" value="{{ old('airline_name') }}" required autofocus>

                                @if ($errors->has('airline_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('airline_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


  						<div class="form-group row">
                            <label for="airline_photo" class="col-md-4 col-form-label text-md-right">{{ __('Foto de la Aerolinea') }}</label>

                            <div class="col-md-6">
                                <input type="file" id="airline_photo" type="text" class="form-control" name="airline_photo" required>
                            </div>
                        </div>

						<div class="form-group row">
                            <label for="airline_webpage" class="col-md-4 col-form-label text-md-right">{{ __('Pagina Web') }}</label>

                            <div class="col-md-6">
                                <input id="airline_webpage" type="text" class="form-control" name="airline_webpage" required>
                            </div>
                        </div>


                    <div class="form-group row">
                            <label for="round_flidht" class="col-md-4 col-form-label text-md-right">{{ __('Vuelo redondo') }}</label>

                            <div class="col-md-6">
                            <select id="round_flidht" type="text" class="form-control" name="round_flidht" >
                                <option value="1">Si</option>
                                <option value="0">No</option>
                            </select>
                            </div>
                        </div>




             			<div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Guardar') }}
                                </button>
                            </div>
                        </div>



                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection