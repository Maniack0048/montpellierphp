@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row justify-content-center">
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Nombre</th>
      <th scope="col">Apellido Paterno</th>
      <th scope="col">Apellido MAterno</th>
      <th scope="col">Correo</th>
      <th scope="col">Acciones</th>
    </tr>
  </thead>
  <tbody>
  	@foreach($users as $user)

    <tr>
      <td>{{$user->name}}</td>
  <td>{{$user->last_name}}</td>
  <td>{{$user->second_surname}}</td>
  <td>{{$user->email}}</td>

            <td>
        <a class="btn btn-info btn-xs" href="{{route('users.edit',$user->id)}}">Editar</a>

        <form style="display:inline" method="POST" action="{{route('users.destroy',$user->id)}}">
          {!! csrf_field() !!}
          {!! method_field('DELETE') !!}
          
          <button class="btn btn-danger" type="submit">Eliminar</button>


        </form>
      </td>
  
    </tr>

    @endforeach
</tbody>
</table>
</div>
</div>
@endsection