@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Registro de Hotel') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('hotels.store') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="hotel_name" class="col-md-4 col-form-label text-md-right">{{ __('Nombre del hotel') }}</label>

                            <div class="col-md-6">
                                <input id="hotel_name" type="text" class="form-control{{ $errors->has('hotel_name') ? ' is-invalid' : '' }}" name="hotel_name" value="{{ old('hotel_name') }}" required autofocus>

                                @if ($errors->has('hotel_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('hotel_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


  						<div class="form-group row">
                            <label for="address" class="col-md-4 col-form-label text-md-right">{{ __('Dirección') }}</label>

                            <div class="col-md-6">
                                <input id="hotel_address" type="text" class="form-control" name="hotel_address" required>
                            </div>
                        </div>

						<div class="form-group row">
                            <label for="web_page" class="col-md-4 col-form-label text-md-right">{{ __('Pagina Web') }}</label>

                            <div class="col-md-6">
                                <input id="web_page" type="text" class="form-control" name="web_page" required>
                            </div>
                        </div>


             			<div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Guardar') }}
                                </button>
                            </div>
                        </div>



                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


