@extends('layouts.app')

@section('content')



<div class="container">
    <div class="row justify-content-center">
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Nombre</th>
      <th scope="col">Foto</th>
      <th scope="col">Acciones</th>
    </tr>
  </thead>
  <tbody>
  	@foreach($catalogs as $catalog)

    <tr>
      <td>{{$catalog->catalog_name}}</td>


      <td><img src="/montpellier/storage/app/{{($catalog->catalog_image)}}"></td>


    <td>
        <a class="btn btn-info btn-xs" href="{{route('catalogs.edit',$catalog->id)}}">Editar</a>

        <form style="display:inline" method="POST" action="{{route('catalogs.destroy',$catalog->id)}}">
          {!! csrf_field() !!}
          {!! method_field('DELETE') !!}
          
          <button class="btn btn-danger" type="submit">Eliminar</button>


        </form>
      </td>


     
    </tr>

    @endforeach
</tbody>
</table>
</div>
</div>
@endsection