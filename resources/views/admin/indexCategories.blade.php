@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row justify-content-center">
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Nombre</th>

      <th scope="col">Acciones</th>
      
    </tr>
  </thead>
  <tbody>
  	@foreach($categories as $category)

    <tr>
      <td>{{$category->category_pack_name}}</td>
     

          <td>
        <a class="btn btn-info btn-xs" href="{{route('categories.edit',$category->id)}}">Editar</a>

        <form style="display:inline" method="POST" action="{{route('categories.destroy',$category->id)}}">
          {!! csrf_field() !!}
          {!! method_field('DELETE') !!}
          
          <button class="btn btn-danger" type="submit">Eliminar</button>


        </form>
      </td>
     

    </tr>

    @endforeach
</tbody>
</table>
</div>
</div>
@endsection