@extends('layouts.app')

@section('content')



<div class="container">
    <div class="row justify-content-center">
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Nombre</th>
      <th scope="col">Vuelo redondo</th>
      <th scope="col">Pagina web</th>
      <th scope="col">Foto</th>
      <th scope="col">Acciones</th>
    </tr>
  </thead>
  <tbody>
  	@foreach($airlines as $airline)

    <tr>
      <td>{{$airline->airline_name}}</td>
@if($airline->round_flidht==1)
      <td>Si</td>
@else

  <td>No</td>
@endif
      <td>{{$airline->airline_webpage}}</td>

      <td><img src="/montpellier/storage/app/{{($airline->airline_photo)}}"></td>


    <td>
        <a class="btn btn-info btn-xs" href="{{route('airlines.edit',$airline->id)}}">Editar</a>

        <form style="display:inline" method="POST" action="{{route('airlines.destroy',$airline->id)}}">
          {!! csrf_field() !!}
          {!! method_field('DELETE') !!}
          
          <button class="btn btn-danger" type="submit">Eliminar</button>


        </form>
      </td>


     
    </tr>

    @endforeach
</tbody>
</table>
</div>
</div>
@endsection