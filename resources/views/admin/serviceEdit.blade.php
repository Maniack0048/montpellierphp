@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Actualizacion de Servicios de Servicios') }}</div>

                <div class="card-body">
                    <form method="POST" enctype="multipart/form-data" action="{{ route('services.update',$service->id) }}">
                            {!! csrf_field() !!}
          {!! method_field('PUT') !!}

                        <div class="form-group row">
                            <label for="service_name" class="col-md-4 col-form-label text-md-right">{{ __('Nombre de la Servicio') }}</label>

                            <div class="col-md-6">
                                <input id="service_name" type="text" class="form-control{{ $errors->has('service_name') ? ' is-invalid' : '' }}" name="service_name" value="{{ $service->service_name }}" required autofocus>

                                @if ($errors->has('service_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('service_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="service_description" class="col-md-4 col-form-label text-md-right">{{ __('Descripcion') }}</label>

                            <div class="col-md-6">
                                <input id="service_description" type="text" class="form-control" name="service_description" value="{{$service->service_description}}" required>
                            </div>
                        </div>


  						<div class="form-group row">
                            <label for="service_icon" class="col-md-4 col-form-label text-md-right">{{ __('Icono') }}</label>

                            <div class="col-md-6">
                                <input type="file" id="service_icon" type="text" class="form-control" name="service_icon" >
                            </div>
                        </div>


              



             			<div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Guardar') }}
                                </button>
                            </div>
                        </div>



                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection