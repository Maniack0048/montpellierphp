@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">

<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Nombre</th>
      <th scope="col">Descripcion</th>
      <th scope="col">Imagen Principal</th>
      <th scope="col">Precio desde</th>
       <th scope="col">Fecha de inicio</th>
        <th scope="col">Fecha fin</th>
      <th scope="col">Tipo de paquete</th>
        <th scope="col">Destacado</th>
       <th scope="col">Duracion dias</th>
        <th scope="col">Duracion Noches</th>    
  <th scope="col">Status</th>
  <th scope="col">Categoria</th>
    <th scope="col">Catalogo</th>
      <th scope="col">Pagina</th>
<th scope="col">Acciones</th>
    </tr>
  </thead>
  <tbody>
  	@foreach($packages as $package)

    <tr>

      <td>{{$package->pack_name}}</td>
      <td>{{$package->description}}</td>

     <td><img src="/montpellier/storage/app/{{($package->main_image)}}"></td>

 <td>{{$package->price_from}}</td>
  <td>{{$package->start_date}}</td>
   <td>{{$package->end_date}}</td>
    <td>{{$package->pack_type}}</td>
     
@if($package->outstanding==1)
      <td>Destacado</td>
@else

  <td>No destacado</td>
@endif
    
  

  <td>{{$package->duration_days}}</td>
   <td>{{$package->duration_nights}}</td>
    
@if($package->pack_status==1)
      <td>Activo</td>
@else

  <td>Inactivo</td>
@endif



      <td>{{$package->category_pack_name}}</td>

 <td>{{$package->catalog_name}}</td>

<td>{{$package->page_number}}</td>
     <td>
        <a class="btn btn-info btn-xs" href="{{route('packages.edit',$package->id)}}">Editar</a>

        <form style="display:inline" method="POST" action="{{route('packages.destroy',$package->id)}}">
          {!! csrf_field() !!}
          {!! method_field('DELETE') !!}
          
          <button class="btn btn-danger" type="submit">Eliminar</button>


        </form>
      </td>
     
    </tr>

    @endforeach
</tbody>
</table>
</div>
</div>
@endsection