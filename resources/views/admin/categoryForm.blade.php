@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Registro de Catalogo') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('categories.store') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="category_pack_name" class="col-md-4 col-form-label text-md-right">{{ __('Nombre de la Categoria') }}</label>

                            <div class="col-md-6">
                                <input id="category_pack_name" type="text" class="form-control{{ $errors->has('category_pack_name') ? ' is-invalid' : '' }}" name="category_pack_name" value="{{ old('category_pack_name') }}" required autofocus>

                                @if ($errors->has('category_pack_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('category_pack_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>



						


             			<div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Guardar') }}
                                </button>
                            </div>
                        </div>



                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection