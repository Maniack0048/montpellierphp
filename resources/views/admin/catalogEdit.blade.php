@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Registro de Catalogo') }}</div>

                <div class="card-body">
                    <form method="POST" enctype="multipart/form-data" action="{{ route('catalogs.update',$catalog->id) }}">
                       
                              {!! csrf_field() !!}
          {!! method_field('PUT') !!}

                        <div class="form-group row">
                            <label for="catalog_name" class="col-md-4 col-form-label text-md-right">{{ __('Nombre del Catalogo') }}</label>

                            <div class="col-md-6">
                                <input id="catalog_name" type="text" class="form-control{{ $errors->has('catalog_name') ? ' is-invalid' : '' }}" name="catalog_name" value="{{ $catalog->catalog_name}}" required autofocus>

                                @if ($errors->has('catalog_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('catalog_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


  						<div class="form-group row">
                            <label for="catalog_image" class="col-md-4 col-form-label text-md-right">{{ __('Foto del Catalogo') }}</label>

                            <div class="col-md-6">
                                <input type="file" id="catalog_image" type="text" class="form-control" name="catalog_image">
                            </div>
                        </div>

						


             			<div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Actualizar') }}
                                </button>
                            </div>
                        </div>



                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection