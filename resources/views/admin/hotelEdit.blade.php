@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Actualizacion de Hotel') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('hotels.update',$hotel->id) }}">
                                                     {!! csrf_field() !!}
          {!! method_field('PUT') !!}

                        <div class="form-group row">
                            <label for="hotel_name" class="col-md-4 col-form-label text-md-right">{{ __('Nombre del hotel') }}</label>

                            <div class="col-md-6">
                                <input id="hotel_name" type="text" class="form-control{{ $errors->has('hotel_name') ? ' is-invalid' : '' }}" name="hotel_name" value="{{ $hotel->hotel_name }}" required autofocus>

                                @if ($errors->has('hotel_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('hotel_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


  						<div class="form-group row">
                            <label for="address" class="col-md-4 col-form-label text-md-right">{{ __('Dirección') }}</label>

                            <div class="col-md-6">
                                <input id="hotel_address" type="text" class="form-control" name="hotel_address" required value="{{$hotel->hotel_address}}">
                            </div>
                        </div>

						<div class="form-group row">
                            <label for="web_page" class="col-md-4 col-form-label text-md-right">{{ __('Pagina Web') }}</label>

                            <div class="col-md-6">
                                <input id="web_page" type="text" class="form-control" name="web_page" value="{{$hotel->web_page}}" required>
                            </div>
                        </div>


             			<div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Actualizar') }}
                                </button>
                            </div>
                        </div>



                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


